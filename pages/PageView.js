import React, { Component } from "react";
import { StyleSheet, WebView } from "react-native";

export default class PageView extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;

    return {
      title: params ? params.title : "Wikipedia View"
    };
  };

  render() {
    const uri = this.props.navigation.getParam(
      "url",
      "https://en.wikipedia.org/wiki/Main_Page"
    );

    return (
      <WebView
        source={{ uri }}
        startInLoadingState={true}
        style={{ flex: 1 }}
      />
    );
  }
}
