import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableWithoutFeedback,
  Keyboard,
  TextInput,
  TouchableOpacity,
  FlatList
} from "react-native";

export default class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: []
    };
  }

  static navigationOptions = {
    title: "Home"
  };

  submitTerms(event) {
    if (event.nativeEvent.text) {
      const baseRequest =
        "https://en.wikipedia.org/w/api.php?action=query&format=json&prop=info&generator=search&inprop=url&gsrsearch=";

      fetch(baseRequest + event.nativeEvent.text)
        .then(resp => resp.json())
        .then(this.parseData.bind(this))
        .catch(console.log);
    }
  }

  parseData(data) {
    const pagesSource = data.query.pages;
    const pages = [];

    for (const page in pagesSource) {
      const pageData = pagesSource[page];

      pages.push({
        url: pageData.fullurl,
        title: pageData.title,
        key: `${pageData.pageid}`,
        index: pageData.index
      });
    }

    this.setState({
      data: pages.sort((a, b) => a.index - b.index)
    });
  }

  _renderItem(renderItem) {
    const { title, url } = renderItem.item;

    return (
      <TouchableOpacity
        style={styles.listItemWrapper}
        onPress={() =>
          this.props.navigation.navigate("PageView", { title, url })
        }
      >
        <Text style={styles.listItem}>{title}</Text>
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <View style={styles.screenWrap}>
          <View style={styles.homeHeader}>
            <Text style={{ fontSize: 20 }}>
              Please enter your topic of interest:
            </Text>

            <TextInput
              placeholder="Biggest Dogs..."
              returnKeyType="go"
              onSubmitEditing={this.submitTerms.bind(this)}
              style={styles.headerInput}
            />
          </View>

          <ScrollView style={{ flexGrow: 3 }}>
            <FlatList
              data={this.state.data}
              renderItem={this._renderItem.bind(this)}
            />
          </ScrollView>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  screenWrap: {
    backgroundColor: "white",
    flex: 1
  },
  homeHeader: {
    alignItems: "center",
    borderBottomWidth: 2,
    borderColor: "white",
    flex: 1,
    justifyContent: "center"
  },
  headerInput: {
    borderColor: "lightgrey",
    borderBottomWidth: 3,
    fontSize: 20,
    marginTop: 20,
    padding: 10,
    width: "80%"
  },
  listItemWrapper: {
    backgroundColor: "#FAFAFA",
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: "white"
  },
  listItem: {
    alignSelf: "center",
    color: "black",
    fontSize: 20,
    margin: 15,
    marginLeft: 25
  }
});
