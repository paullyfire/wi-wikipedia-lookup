## Exercise 2

Create an app that accepts keywords/texts, performs a GET request on Wikipedia and displays the results in a list just below the text input.
Afterwards, when a user clicks on row, he should be navigated to a detailed screen which has a navigation bar (with its title being the Wikipedia page) and a “WebView” component to load that page.

* [Available Scripts](#available-scripts)
  * [npm start](#npm-start)
  * [npm run ios](#npm-run-ios)
  * [npm run android](#npm-run-android)

#### `npm run ios`

Like `npm start`, but also attempts to open your app in the iOS Simulator if you're on a Mac and have it installed.

#### `npm run android`

Like `npm start`, but also attempts to open your app on a connected Android device or emulator. Requires an installation of Android build tools (see [React Native docs](https://facebook.github.io/react-native/docs/getting-started.html) for detailed setup).
