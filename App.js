import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  Keyboard,
  KeyboardAvoidingView,
  TextInput
} from "react-native";
import { createStackNavigator } from "react-navigation";
import Home from "./pages/Home";
import PageView from "./pages/PageView";

export default class App extends React.Component {
  render() {
    return <MainStack />;
  }
}

const MainStack = createStackNavigator(
  {
    Home: Home,
    PageView: PageView
  },
  {
    initialRouteName: "Home"
  }
);
